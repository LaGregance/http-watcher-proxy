# HTTP Watcher Proxy

It's always dificult to inspect HTTP requests when you are developing on portable device.  
On the web it's easy, just open the developer console and go to network. On cell phone most of time we need to log most of data on the console and it's a head shot to find the information we want.  
I wished to find this web experience on cell phones, that's what HTTP Watcher Proxy was born.

## What is HTTP Watcher Proxy

HTTP Watcher Proxy is a proxy that will intercept your request and display them in an user-friendly interface.
![](ressources/screenshot.png)

## Setup

```bash
git clone https://gitlab.com/LaGregance/http-watcher-proxy.git
yarn install
WATCH_URL=<URL you want to watch> PORT=<Proxy PORT> yarn start # Or define envirionment variable...
```

Next step you need to setup you app to target the proxy instead of the API URL.  

For exemple if my API is https://api.mywebsite.com, I will start the proxy like that
`WATCH_URL=https://api.mywebsite.com PORT=5000 yarn start`, and I will set the API URL on my app to `http://localhost:5000` (or my public IP if the mobile is not a simulator).  

Enjoy.

## Pros / Cons 

Pros :
- Easy to setup
- User friendly

Cons :
- We can only watch one URL by proxy, if we want to watch more than one URL we need to setup multiple proxy 
- Hard to use trought 4G, the best way is to use it with mobile simulator or device connected to same network
