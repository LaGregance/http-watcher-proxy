import express from 'express';
import bodyParser from 'body-parser';
import Proxy from './proxy';
import axios, { AxiosInstance } from 'axios';
import { Server } from 'http';

describe('Proxy test', () => {
  const WATCH_URL = 'http://localhost:5001';
  const WATCH_PORT = 5001;
  const PROXY_PORT = 5002;

  let fakeApp: express.Express;
  let fakeServer: Server;
  let proxy: Proxy;
  let client: AxiosInstance;

  beforeAll(async () => {
    fakeApp = express();
    fakeApp.use(bodyParser.urlencoded({ extended: true }));
    fakeApp.use(bodyParser.json());

    fakeApp.get('/', (req, res) => {
      res.json({
        hello: 'world',
        value: 12,
        obj: {
          sub: 'item',
        },
        array: [12, '13', 14],
      });
      res.end();
    });

    fakeApp.post('/item', (req, res) => {
      res.json(req.body);
    });

    await new Promise(
      (res) => (fakeServer = fakeApp.listen(WATCH_PORT, () => res()))
    );

    proxy = new Proxy(WATCH_URL, PROXY_PORT);
    await proxy.listen();

    client = axios.create({
      baseURL: 'http://localhost:' + PROXY_PORT,
    });
  });

  afterAll(() => {
    proxy.close();
    fakeServer.close();
  });

  it('GET JSON', async () => {
    const response = await client.get('/');

    expect(response.data).toEqual({
      hello: 'world',
      value: 12,
      obj: {
        sub: 'item',
      },
      array: [12, '13', 14],
    });
  });

  it('POST JSON', async () => {
    const response = await client.post('/item', { hello: 'world' });

    expect(response.data).toEqual({
      hello: 'world',
    });
  });

  it('Check GET callback', (done) => {
    proxy.setCallback((req, res) => {
      try {
        expect(res.statusCode).toEqual(200);
        expect(req.method).toEqual('GET');
        expect(req.originalUrl).toEqual('/');
        expect(req.headers['content-type']).toBeUndefined();
        proxy.setCallback(undefined);
        done();
      } catch (e) {
        done(e);
      }
    });
    client.get('/').catch(() => done());
  });

  it('Check POST callback', (done) => {
    proxy.setCallback((req, res) => {
      try {
        expect(res.statusCode).toEqual(200);
        expect(req.method).toEqual('POST');
        expect(req.originalUrl).toEqual('/item');
        expect(req.headers['content-type']).toEqual(
          'application/json;charset=utf-8'
        );
        proxy.setCallback(undefined);
        done();
      } catch (e) {
        done(e);
      }
    });
    client.post('/item', { hello: 'world' }).catch(() => done());
  });
});
