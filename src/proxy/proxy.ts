import express from 'express';
import proxy from 'express-http-proxy';
import { ParamsDictionary } from 'express-serve-static-core';
import QueryString from 'qs';
import bodyParser from 'body-parser';
import { Server } from 'http';

type RequestCallback = (
  req: express.Request<ParamsDictionary, any, any, QueryString.ParsedQs>,
  res: express.Response<any>,
  resBuffer: Buffer
) => void;

export default class Proxy {
  private app: express.Express;
  private server?: Server;
  private callback?: RequestCallback;

  constructor(private watchBaseURL: string, private port: number) {
    this.app = express();

    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());

    this.app.use(
      proxy(this.watchBaseURL, {
        proxyReqPathResolver: (req) => req.originalUrl,
        userResDecorator: (proxyRes, proxyResData, userReq) => {
          try {
            this.callback && this.callback(userReq, proxyRes, proxyResData);
          } catch (e) {
            console.error(e);
          }
          return proxyResData;
        },
      })
    );
  }

  setCallback(callback: RequestCallback | undefined) {
    this.callback = callback;
  }

  async listen() {
    return new Promise<void>((res) => {
      this.server = this.app.listen(this.port, () => {
        res();
      });
    });
  }

  close() {
    this.server && this.server.close();
  }
}
