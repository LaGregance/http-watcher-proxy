import MainWindow from './widgets/main-window';

const win = new MainWindow();
win.show();

(global as any).win = win;
