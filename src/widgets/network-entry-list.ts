import { ItemDataRole, QTreeWidget, QTreeWidgetItem } from '@nodegui/nodegui';
import * as uuid from 'uuid';
import { NetworkEntry } from '../models/network-entry';

export default class NetworkEntryList extends QTreeWidget {
  private list: Map<string, NetworkEntry>;

  constructor() {
    super();
    this.list = new Map();

    this.setInlineStyle('background-color: white;');
    this.setHeaderLabels(['Status', 'Method', 'File', 'Type']);
  }

  addItem(model: NetworkEntry) {
    const { res, req } = model;
    const item = new QTreeWidgetItem(this, [
      res.statusCode.toString(),
      req.method,
      req.originalUrl,
      req.headers['content-type'] || 'unknow',
    ]);
    const id = uuid.v4();
    item.setData(0, ItemDataRole.DecorationRole, id);
    this.list.set(id, model);
    return item;
  }

  getModelWithItem(item: QTreeWidgetItem) {
    const id = item.data(0, ItemDataRole.DecorationRole).toString();
    return this.list.has(id) ? this.list.get(id) : undefined;
  }
}
