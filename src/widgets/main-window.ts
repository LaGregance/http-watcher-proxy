import express from 'express';
import {
  Direction,
  QBoxLayout,
  QMainWindow,
  QMenuBar,
  QWidget,
} from '@nodegui/nodegui';
import { ParamsDictionary } from 'express-serve-static-core';
import QueryString from 'qs';
import Proxy from '../proxy/proxy';
import NetworkEntryList from './network-entry-list';
import NetworkEntryDetails from './network-entry-details/network-entry-details';

export default class MainWindow extends QMainWindow {
  private requestList: NetworkEntryList;
  private requestDetails: NetworkEntryDetails;
  private proxy: Proxy;

  constructor() {
    super();

    const WATCH_URL = process.env.WATCH_URL;
    const PORT: number | undefined = process.env.PORT
      ? parseInt(process.env.PORT)
      : undefined;

    if (!WATCH_URL || !PORT || PORT <= 0) {
      throw Error('You have to define WATCH_URL & PORT variables');
    }

    this.setWindowTitle('HTTP Watcher Proxy');
    this.resize(800, 600);

    const centralWidget = new QWidget();
    const layout = new QBoxLayout(Direction.LeftToRight);
    layout.setContentsMargins(0, 0, 0, 0);
    layout.setSpacing(0);
    centralWidget.setLayout(layout);

    this.requestList = new NetworkEntryList();

    this.requestDetails = new NetworkEntryDetails();

    layout.addWidget(this.requestList, 2);
    layout.addWidget(this.requestDetails, 1);

    this.setCentralWidget(centralWidget);

    this.proxy = new Proxy(WATCH_URL, PORT);
    this.proxy.setCallback(this.requestCallback.bind(this));
    this.proxy.listen().then(() => console.info(`Proxy open on port ${PORT}`));

    this.requestList.addEventListener('currentItemChanged', (current) => {
      const request = this.requestList.getModelWithItem(current);
      request && this.requestDetails.setSelectedRequest(request);
    });
  }

  requestCallback(
    req: express.Request<ParamsDictionary, any, any, QueryString.ParsedQs>,
    res: express.Response<any>,
    resBuffer: Buffer
  ) {
    this.requestList.addItem({
      req,
      res,
      resBuffer,
    });
  }
}
