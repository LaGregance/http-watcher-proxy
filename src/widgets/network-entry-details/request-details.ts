import { QTreeWidget, QTreeWidgetItem } from '@nodegui/nodegui';
import { NetworkEntry } from '../../models/network-entry';

export default class RequestDetails extends QTreeWidget {
  constructor() {
    super();
    this.setHeaderHidden(true);
  }

  setup(entry: NetworkEntry) {
    this.clear();

    for (const [key, value] of Object.entries(entry.req.body)) {
      const item = new QTreeWidgetItem(this, [key]);
      new QTreeWidgetItem(item, [`${value}`]);
    }
  }
}
