import { QTreeWidget, QTreeWidgetItem } from '@nodegui/nodegui';
import { NetworkEntry } from '../../models/network-entry';

export default class ResponseDetails extends QTreeWidget {
  constructor() {
    super();
    this.setHeaderHidden(true);
  }

  setup(entry: NetworkEntry) {
    this.clear();

    const resStr = entry.resBuffer.toString();

    try {
      const resJson = JSON.parse(resStr);

      const rootItem = this.createLabelItem(undefined, resJson);
      if (Array.isArray(resJson)) {
        this.createArrayTree(resJson, rootItem);
      } else {
        this.createObjectTree(resJson, rootItem);
      }
    } catch (e) {
      new QTreeWidgetItem(this, [resStr]);
    }
  }

  createLabelItem(key?: string, obj?: any, parent?: QTreeWidgetItem) {
    const type = (() => {
      if (Array.isArray(obj)) return '[Array]';
      else if (typeof obj === 'object') return '{Object}';
      return '';
    })();
    const label = key ? `${key} ${type}` : type;

    const item = parent
      ? new QTreeWidgetItem(parent, [label])
      : new QTreeWidgetItem(this, [label]);
    return item;
  }

  createArrayTree(array: any[], parent?: QTreeWidgetItem) {
    for (let i = 0; i < array.length; i++) {
      const item = this.createLabelItem(i.toString(), array[i], parent);

      if (Array.isArray(array[i])) {
        this.createArrayTree(array[i], item);
      } else if (typeof array[i] === 'object') {
        this.createObjectTree(array[i], item);
      } else {
        new QTreeWidgetItem(item, [`${array[i]}`]);
      }
    }
  }

  createObjectTree(obj: object, parent?: QTreeWidgetItem) {
    for (const [key, value] of Object.entries(obj)) {
      const item = this.createLabelItem(key, value, parent);

      if (Array.isArray(value)) {
        this.createArrayTree(value, item);
      } else if (typeof value === 'object') {
        this.createObjectTree(value, item);
      } else {
        new QTreeWidgetItem(item, [`${value}`]);
      }
    }
  }
}
