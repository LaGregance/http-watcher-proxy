import { QTreeWidget, QTreeWidgetItem } from '@nodegui/nodegui';
import { NetworkEntry } from '../../models/network-entry';

export default class HeaderDetails extends QTreeWidget {
  constructor() {
    super();
    this.setHeaderHidden(true);
  }

  setup(entry: NetworkEntry) {
    this.clear();

    const response = new QTreeWidgetItem(this, ['Response Header']);
    const responseHeader = (entry.res as any).headers as object;
    this.showHeaders(response, responseHeader);

    const request = new QTreeWidgetItem(this, ['Request Header']);
    this.showHeaders(request, entry.req.headers);
  }

  private showHeaders(parent: QTreeWidgetItem, headers: object) {
    for (const [key, value] of Object.entries(headers)) {
      const item = new QTreeWidgetItem(parent, [key]);
      new QTreeWidgetItem(item, [`${value}`]);
    }
  }
}
