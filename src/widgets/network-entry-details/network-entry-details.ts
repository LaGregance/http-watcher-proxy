import { QIcon, QTabWidget } from '@nodegui/nodegui';
import { NetworkEntry } from '../../models/network-entry';
import HeaderDetails from './header-details';
import RequestDetails from './request-details';
import ResponseDetails from './response-details';

export default class NetworkEntryDetails extends QTabWidget {
  private headerDetails: HeaderDetails;
  private requestDetails: RequestDetails;
  private responseDetails: ResponseDetails;

  constructor() {
    super();

    this.headerDetails = new HeaderDetails();
    this.requestDetails = new RequestDetails();
    this.responseDetails = new ResponseDetails();
    this.addTab(this.headerDetails, new QIcon(), 'Headers');
    this.addTab(this.requestDetails, new QIcon(), 'Request');
    this.addTab(this.responseDetails, new QIcon(), 'Response');
  }

  setSelectedRequest(entry: NetworkEntry) {
    this.headerDetails.setup(entry);
    this.requestDetails.setup(entry);
    this.responseDetails.setup(entry);
  }
}
