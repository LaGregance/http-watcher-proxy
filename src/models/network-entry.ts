import express from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import QueryString from 'qs';

export interface NetworkEntry {
  req: express.Request<ParamsDictionary, any, any, QueryString.ParsedQs>;
  res: express.Response<any>;
  resBuffer: Buffer;
}
